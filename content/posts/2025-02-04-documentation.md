---
title: "La documentation c'est la base !"
description: ""
date: "2025-02-04"
tags: ["doc","sysadmin"]
draft: true
#tldr: ""
---

Dans notre métier de sysadmin il y a comme une petite musique qui tourne: *les administrateurs réseaux ou systèmes n'aiment pas documenter !* 


C'est vrai que lorsqu'on monte une nouvelle infrastructure, l'enthousiasme de découvrir une nouvelle technologie, un nouveau hardware,
un nouveau projet (libre) l'emporte sur la trace écrite qu'on va maintenir pour décrire l'installation.
Il peut arriver de se trouver sous pression dans le déploiement d'une plateforme, et on ne prête pas attention à la documentation.

## Les problèmes qui se posent

Un sysadmin qui ne documente pas, même très peu, c'est une personne qui a tout dans la tête. Arrive alors des soucis concernant:
  - la continuité de service,
  - la charge mentale des collègues,
  - les risques dans le suivi des maintenances préventives ou correctives. 

Il n'y a que l'absence du sysadmin qui permettra à l'entreprise de comprendre l'impacte d'un manque de la documentation. 


## Mais comment documenter ?

La question que je me pose régulièrement c'est **comment documenter efficacement ?** 

Je veux dire que si mon équipe et moi partons en vacances 3 mois, On laisse à une autre équipe la doc + les fichiers d'accès (nota: dont l'utilisation est décrite dans la doc, elle même soumis à un mot de passe...)   que se passe t'il ? Ont ils toutes les informations pour assurer le service attendu ?


### En première approche

Le plus simple, **documenter à l'installation**, c'est **la fameuse fiche d'installation**. Celle que tout bon stagiaire rédige à la perfection avec toute un tas de copier-coller de sa console. Cette fiche, on s'en sert pour la filer à un collègue qui veut déployer la même infra, on la ressort à l'occasion de la mise à jour pour savoir comment le problème de dépendances a été résolu ou on l'utilise pour capitaliser sur l'installation d'un composant.

Cette fiche est très statique, typique du dépôt [GIT](https://git-scm.com/) qui contient les scripts de déploiements.

